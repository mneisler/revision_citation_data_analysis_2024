# revision_citation_data_analysis_2024

Generate a sample of citations added by revisions across several different wikis and include data on editor experience and the revert rate or revision risk of revisions with that citation.

This dataset will be used to explore citation usage by different editor types. Are there common trends in the types of citations that newcomers add vs more experienced users? What types of citations are frequently reverted?

Analysis can help provide insights into source reliability and help identify policies that would be helpful to provide newcomers adding a citation to an edit.

Completed as part of the Wikimedia Hackathon 2024
[Task](https://phabricator.wikimedia.org/T363616)